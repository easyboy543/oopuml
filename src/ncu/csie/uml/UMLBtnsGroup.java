package ncu.csie.uml;

import java.util.Vector;

public class UMLBtnsGroup{

	private static Vector<UMLBtn> umlBtns = new Vector<>();
	
	public static Vector<UMLBtn> getUmlBtns() {
		return umlBtns;
	}

	public static void resetIcons(){
		for(int i=0; i<umlBtns.size(); ++i){
			umlBtns.get(i).resetIcon();
		}
	}
}
