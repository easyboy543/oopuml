package ncu.csie.uml;

import java.awt.event.ActionEvent;

import ncu.csie.uml.gfx.Assets;
import ncu.csie.uml.mode.ComLineMode;
import ncu.csie.uml.mode.Mode;

public class ComLineBtn extends UMLBtn {

	public ComLineBtn(String name) {
		super(name);
		resetIcon();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		UMLBtnsGroup.resetIcons();
		setIcon(Assets.btnComLined);
		Mode.setMode(new ComLineMode());
	}

	@Override
	public void resetIcon() {
		setIcon(Assets.btnComLine);
	}

}
