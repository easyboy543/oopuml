package ncu.csie.uml;

import java.awt.event.ActionEvent;

import ncu.csie.uml.gfx.Assets;
import ncu.csie.uml.mode.ClassMode;
import ncu.csie.uml.mode.Mode;

public class ClassBtn extends UMLBtn {

	public ClassBtn(String name) {
		super(name);
		setIcon(Assets.btnClass);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		UMLBtnsGroup.resetIcons();
		setIcon(Assets.btnClassed);		
		Mode.setMode(new ClassMode());
		
	}

	@Override
	public void resetIcon() {
		setIcon(Assets.btnClass);
	}

}
