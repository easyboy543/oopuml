package ncu.csie.uml;

import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import ncu.csie.uml.shape.BasicObjectManager;

public abstract class UMLMenuItem extends JMenuItem implements ActionListener{

	protected UMLJPanel umlJPanel;
	
	public UMLMenuItem(String text, UMLJPanel umlJPanel) {
		super(text);
		addActionListener(this);
		this.umlJPanel = umlJPanel;
	}
	
}
