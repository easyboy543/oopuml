package ncu.csie.uml;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import ncu.csie.uml.gfx.Assets;

public class UMLEditor extends JFrame {

	private JMenuBar menuBar;
	private JMenu menuFile, menuEdit;
	private RenameItem itemRename;
	
	
	private JPanel leftPanel;
	private UMLJPanel umlJpanel;
	private UMLBtn usecaseBtn, classBtn, assLineBtn, genLineBtn, comLineBtn, selectBtn;
	
		

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Assets.init();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UMLEditor frame = new UMLEditor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UMLEditor() {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenDim = toolkit.getScreenSize();
		setSize(screenDim.width, screenDim.height);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
						
				
		leftPanel = new JPanel();
		leftPanel.setLocation(0, 0);
		leftPanel.setSize(screenDim.width/5, screenDim.height);	
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		
		usecaseBtn = new UseCaseBtn("");
		classBtn = new ClassBtn("");
		assLineBtn = new AssLineBtn("");
		comLineBtn = new ComLineBtn("");
		genLineBtn = new GenLineBtn("");
		selectBtn = new SelectBtn("");
		
		UMLBtnsGroup.getUmlBtns().add(usecaseBtn);
		UMLBtnsGroup.getUmlBtns().add(classBtn);
		UMLBtnsGroup.getUmlBtns().add(assLineBtn);
		UMLBtnsGroup.getUmlBtns().add(genLineBtn);
		UMLBtnsGroup.getUmlBtns().add(comLineBtn);
		UMLBtnsGroup.getUmlBtns().add(selectBtn);
		
		
		leftPanel.add(usecaseBtn);
		leftPanel.add(classBtn);
		leftPanel.add(assLineBtn);
		leftPanel.add(genLineBtn);
		leftPanel.add(comLineBtn);
		leftPanel.add(selectBtn);
		
				
		getContentPane().add(leftPanel);
		
		
		umlJpanel = new UMLJPanel();
		umlJpanel.setBackground(Color.WHITE);
		umlJpanel.setLocation(screenDim.width/5, 0);
		umlJpanel.setSize(screenDim.width/5*4, screenDim.height);		
		getContentPane().add(umlJpanel, BorderLayout.CENTER);
		
				
		menuBar = new JMenuBar();
		menuFile = new JMenu("File");
		menuEdit = new JMenu("Edit");
		
		itemRename = new RenameItem("Rename", umlJpanel);
		menuEdit.add(itemRename);
		
		menuBar.add(menuFile);
		menuBar.add(menuEdit);
		
		setJMenuBar(menuBar);
		
	}

}
