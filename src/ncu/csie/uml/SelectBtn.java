package ncu.csie.uml;

import java.awt.event.ActionEvent;

import ncu.csie.uml.gfx.Assets;
import ncu.csie.uml.mode.Mode;
import ncu.csie.uml.mode.SelectMode;

public class SelectBtn extends UMLBtn {

	public SelectBtn(String name) {
		super(name);
		setIcon(Assets.btnSelect);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		UMLBtnsGroup.resetIcons();
		setIcon(Assets.btnSelected);
		Mode.setMode(new SelectMode());
	}

	@Override
	public void resetIcon() {
		setIcon(Assets.btnSelect);		
	}

}
