package ncu.csie.uml;

import java.awt.event.ActionEvent;

import ncu.csie.uml.gfx.Assets;
import ncu.csie.uml.mode.Mode;
import ncu.csie.uml.mode.UseCaseMode;

public class UseCaseBtn extends UMLBtn {

	public UseCaseBtn(String name) {
		super(name);
		resetIcon();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		UMLBtnsGroup.resetIcons();
		setIcon(Assets.btnUseCased);
		Mode.setMode(new UseCaseMode());		
	}

	@Override
	public void resetIcon() {
		setIcon(Assets.btnUseCase);		
	}

}
