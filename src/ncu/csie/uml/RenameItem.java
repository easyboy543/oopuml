package ncu.csie.uml;

import java.awt.event.ActionEvent;
import java.util.Locale;
import java.util.Vector;

import javax.swing.JOptionPane;

import ncu.csie.uml.shape.BasicObject;
import ncu.csie.uml.shape.UMLShape;
import ncu.csie.uml.shape.BasicObjectManager;

public class RenameItem extends UMLMenuItem {

		
	
	public RenameItem(String text, UMLJPanel umlJPanel) {
		super(text, umlJPanel);
		this.umlJPanel = umlJPanel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JOptionPane.setDefaultLocale(Locale.ENGLISH);
		String name = JOptionPane.showInputDialog(null, "Please enter a input name", "Rename",
				JOptionPane.PLAIN_MESSAGE);  
		Vector<BasicObject> basicObjs = BasicObjectManager.getInstance().getBasicObjects();
		for(int i=0; i<basicObjs.size(); ++i){			
			if(basicObjs.get(i).isSelected()){
				basicObjs.get(i).setName(name);
			}			
		}
		this.umlJPanel.repaint();
	}

}
