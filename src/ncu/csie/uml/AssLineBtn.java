package ncu.csie.uml;

import java.awt.event.ActionEvent;

import ncu.csie.uml.gfx.Assets;
import ncu.csie.uml.mode.AssLineMode;
import ncu.csie.uml.mode.Mode;

public class AssLineBtn extends UMLBtn {
	

	public AssLineBtn(String name) {
		super(name);
		setIcon(Assets.btnAssLine);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		UMLBtnsGroup.resetIcons();
		setIcon(Assets.btnAssLined);
		Mode.setMode(new AssLineMode());
	}

	@Override
	public void resetIcon() {
		setIcon(Assets.btnAssLine);
	}

}
