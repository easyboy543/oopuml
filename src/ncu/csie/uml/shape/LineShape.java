package ncu.csie.uml.shape;

import java.awt.Graphics2D;

public class LineShape extends UMLShape {

	protected Port sd, ed;
		
	public LineShape(Port sd, Port ed) {
		this.sd = sd;
		this.ed = ed;
	}

	@Override
	public void draw(Graphics2D g2d) {		
		int x1 = sd.getRx();
		int y1 = sd.getRy();
		int x2 = ed.getRx();
		int y2 = ed.getRy();
		g2d.drawLine(x1, y1, x2, y2);		
	}

	
}
