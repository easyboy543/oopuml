package ncu.csie.uml.shape;

import java.awt.Graphics2D;

public class UseCaseShape extends BasicObject {

	private static final int WIDTH = 100;
	private static final int HEIGHT = 60;
	
	
	public UseCaseShape(int x, int y) {
		super(x, y, WIDTH, HEIGHT);
		
		this.name = "UseCase";
	}

	@Override
	public void draw(Graphics2D g2d) {
		super.draw(g2d);
		g2d.drawString(this.name, x+30, y+30);
		g2d.drawOval(x, y, width, height);
	}

}
