package ncu.csie.uml.shape;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;


public class GenLineShape extends LineShape {

	private Shape arrow;
	
	public GenLineShape(Port sd, Port ed) {
		super(sd, ed);
		updateArrow();		
	}

	@Override
	public void draw(Graphics2D g2d) {		
		super.draw(g2d);
		updateArrow();
		g2d.draw(arrow);
		
	} 
	
	private void updateArrow() {
		PointyTriangle pointyTriangle = new PointyTriangle();
		
		double rotation = 0f;
                        
        int startX = sd.getRx();
        int startY = sd.getRy();
        int endX = ed.getRx();
        int endY = ed.getRy();
        int deltaX = endX - startX;
        int deltaY = endY - startY;

        rotation = -Math.atan2(deltaX, deltaY);
        rotation = Math.toDegrees(rotation) + 180;
        
        Rectangle bounds = pointyTriangle.getBounds();

        
        AffineTransform at = new AffineTransform();

        at.translate(endX - (bounds.width / 2), endY - (bounds.height / 2));
        at.rotate(Math.toRadians(rotation), bounds.width / 2, bounds.height / 2);
        arrow = new Path2D.Float(pointyTriangle, at);
		
	}

	class PointyTriangle extends Path2D.Float {

	    public PointyTriangle() {
	        moveTo(15, 0);
	        lineTo(30, 15);
	        lineTo(0, 15);
	        lineTo(15, 0);
	    }

	}
	
	
	
}
