package ncu.csie.uml.shape;

import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.util.Vector;

public class BasicObjectManager {

	private Vector<BasicObject> basicObjs = new Vector<>();
		
	
	private static BasicObjectManager basicObjectManager;
	
	private BasicObjectManager(){}
	
	public static synchronized BasicObjectManager getInstance(){
		if(basicObjectManager == null){
			basicObjectManager = new BasicObjectManager();
		}
		return basicObjectManager;
	}

	public Vector<BasicObject> getBasicObjects() {
		return basicObjs;
	}

	public void addBasicObject(BasicObject basic) {
		int depth = countDepth(basic);		
		basic.setDepth(depth);
		basicObjs.add(basic);
	}

	private boolean isOverlap(BasicObject basic1, BasicObject basic2) {		
		Rectangle2D bound1 = new Rectangle2D.Double(basic1.getX(), basic1.getY(), basic1.getWidth(), basic1.getHeight());
		Rectangle2D bound2 = new Rectangle2D.Double(basic2.getX(), basic2.getY(), basic2.getWidth(), basic2.getHeight());
		return bound1.intersects(bound2);
	}

	public Port getClosestPort(Point point) {		
		for(BasicObject obj: basicObjs){
			if(obj.isInBasicObject(point)){
				return obj.getClosestPort(point);
			}
		}		
		return null;
	}

	public void singleSelect(Point point) {
		
		resetSelect();
		BasicObject obj = getSelectObj(point);
		if(obj != null){
			obj.setSelected(true);
		}
		
		
	}

	public void multiSelect(Point sp, Point ep) {
						
		//point is in shape => do not select
		if(getSelectObj(sp) != null){			
			return;
		}
			
		resetSelect();
		
		if(sp.getX() > ep.getX() && sp.getY() > ep.getY()){
			Point tmp = sp;
			sp = ep;
			ep = tmp;
		}
		
		for(int i=0; i<basicObjs.size(); ++i){
			BasicObject obj = basicObjs.get(i);
			if(obj.getX() > sp.getX() && obj.getY() > sp.getY() &&
					(obj.getX()+obj.getWidth())< ep.getX() && (obj.getY()+obj.getHeight()) < ep.getY()){
				obj.setSelected(true);
			}
		}
	}

	
	private void resetSelect(){
		//reset select
		for(BasicObject obj: basicObjs){
			obj.setSelected(false);
		}
	}

	private BasicObject getSelectObj(Point point){
		//select
		BasicObject obj = null;
		int maxDepth = -1;		
		for(int i=0; i<basicObjs.size(); ++i){
			if(basicObjs.get(i).isInBasicObject(point) && basicObjs.get(i).getDepth() > maxDepth){
				maxDepth = basicObjs.get(i).getDepth();
				obj = basicObjs.get(i);
			}			
		}
		return obj;
	}

	public void moveBasicObject(Point sp, Point ep) {
		
		BasicObject obj = getSelectObj(sp);
		
		//when point is not in shape, cancel move 
		if(obj == null){
			return ;
		}
		
		obj.setX((int) ep.getX());
		obj.setY((int) ep.getY());
		
		int depth = countDepth(obj);
		obj.setDepth(depth);
				
	}
	
	
	private int countDepth(BasicObject basic){
		int depth = 0;
		for(int i=0; i<basicObjs.size(); ++i){
			if(isOverlap(basic, basicObjs.get(i))){
				depth++;
			}
		}
		return depth;
	}
			

	
		
	
	
	
}
