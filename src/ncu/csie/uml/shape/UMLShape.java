package ncu.csie.uml.shape;

import java.awt.Graphics2D;

public abstract class UMLShape {	
	
	public abstract void draw(Graphics2D g2d);
	
}
