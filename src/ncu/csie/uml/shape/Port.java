package ncu.csie.uml.shape;

import java.util.Vector;

public class Port {

	private int rx, ry;
	private BasicObject parent;
	private Vector<LineShape> lines;
		
	public Port(int rx, int ry, BasicObject parent) {
		this.rx = rx;
		this.ry = ry;
		this.parent = parent;
		lines = new Vector<>();
	}

	public int getRx() {
		return parent.getX()+rx;
	}

	public int getRy() {
		return parent.getY()+ry;
	}
	
	public void addLine(LineShape lineShape) {
		lines.add(lineShape);
		
	}

	public Vector<LineShape> getLines() {
		
		return lines;
	}

	
		
}
