package ncu.csie.uml.shape;

import java.awt.Graphics2D;
import java.awt.Point;
import java.util.Vector;

public abstract class BasicObject extends UMLShape {

	protected String name;
	protected int x, y;
	protected int width, height;
	protected int depth;
	protected Vector<Port> linkPorts;
	protected boolean isSelected = false;
	
	public BasicObject(int x, int y, int width, int height) {		
		this.x = x;
		this.y = y;				
		this.width = width;
		this.height = height;		
		linkPorts = new Vector<>();
		linkPorts.add(new Port(width/2, 0, this));
		linkPorts.add(new Port(0, height/2, this));
		linkPorts.add(new Port(width/2, height, this));
		linkPorts.add(new Port(width, height/2, this));
		
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	
	public Port getClosestPort(Point ed){								
		
		double min = Double.MAX_VALUE;
		Port port = null;
		for(Port pt: this.linkPorts){
			double sdX = pt.getRx();
			double sdY = pt.getRy();
			if( min > ed.distance(sdX, sdY)){
				min = ed.distance(sdX, sdY);
				port = pt;
			}
		}
		
		return port;
	}
	
	public boolean isInBasicObject(Point p){
		if(p.getX() > this.x && p.getX() < (this.x+this.width) &&
				p.getY() > this.y && p.getY() < (this.y+this.height)){
			return true;
		}
		return false;
	}
	
	@Override
	public void draw(Graphics2D g2d){
		if(isSelected){
			for(int i=0; i<linkPorts.size(); ++i){
				Port port = linkPorts.get(i);
				int x, y;
				x = port.getRx();
				y = port.getRy();
				g2d.fillRect(x-5, y-5, 10, 10);
			}
		}
		for(Port port: linkPorts){			
			for(LineShape line: port.getLines()){
				line.draw(g2d);
			}
		}
	}
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setDepth(int depth) {
		this.depth = depth;
		
	}
	public int getDepth() {
		
		return this.depth;
	}	
}
