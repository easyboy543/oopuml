package ncu.csie.uml.shape;

import java.awt.Graphics2D;
import java.util.Vector;

public class ClassShape extends BasicObject {
		
	private Vector<RPoint> linkPoint;
	private static final int WIDTH = 100;
	private static final int HEIGHT = 100;
	
	
	public ClassShape(int x, int y) {
		super(x, y, WIDTH, HEIGHT);		
		this.name = "Class";
		linkPoint = new Vector<>();
		linkPoint.add(new RPoint(this, 0, height/3));
		linkPoint.add(new RPoint(this, width, height/3));
		linkPoint.add(new RPoint(this, 0, height/3*2));
		linkPoint.add(new RPoint(this, width, height/3*2));
	}		
	
		

	@Override
	public void draw(Graphics2D g2d) {
		super.draw(g2d);	
		g2d.drawString(this.name, x+30, y+20);
		g2d.drawRect(x, y, width, height);
		g2d.drawLine(linkPoint.get(0).getRX(), linkPoint.get(0).getRY(),
				linkPoint.get(1).getRX(), linkPoint.get(1).getRY());
		g2d.drawLine(linkPoint.get(2).getRX(), linkPoint.get(2).getRY(),
				linkPoint.get(3).getRX(), linkPoint.get(3).getRY());
	}

	
	private class RPoint {
		private ClassShape shape;
		private int rx, ry;
		public RPoint(ClassShape shape, int rx, int ry){
			this.shape = shape;
			this.rx = rx;
			this.ry = ry;
		}
		
		public int getRX(){
			return this.shape.x+rx;
		}
		
		public int getRY(){
			return this.shape.y+ry;
		}
	}
}
