package ncu.csie.uml;

import java.awt.event.ActionListener;

import javax.swing.JButton;

public abstract class UMLBtn extends JButton implements ActionListener{
			
	
	
	public UMLBtn(String name){	
		super(name);
		addActionListener(this);
	}
	
	public abstract void resetIcon();
}
