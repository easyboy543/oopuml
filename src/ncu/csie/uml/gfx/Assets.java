package ncu.csie.uml.gfx;

import javax.swing.ImageIcon;

public class Assets {
	public static ImageIcon btnSelect, btnSelected, btnAssLine, btnAssLined, btnGenLine, btnGenLined, btnComLine, btnComLined,
								btnClass, btnClassed, btnUseCase, btnUseCased;
	
	public static void init(){
		btnSelect = new ImageIcon(Assets.class.getResource("/textures/btnSelect.PNG"));
		btnSelected = new ImageIcon(Assets.class.getResource("/textures/btnSelected.PNG"));
		
		btnAssLine = new ImageIcon(Assets.class.getResource("/textures/btnAssLine.PNG"));
		btnAssLined = new ImageIcon(Assets.class.getResource("/textures/btnAssLined.PNG"));
		
		btnGenLine = new ImageIcon(Assets.class.getResource("/textures/btnGenLine.PNG"));
		btnGenLined = new ImageIcon(Assets.class.getResource("/textures/btnGenLined.PNG"));
		
		btnComLine = new ImageIcon(Assets.class.getResource("/textures/btnComLine.PNG"));
		btnComLined= new ImageIcon(Assets.class.getResource("/textures/btnComLined.PNG"));
		
		btnClass  = new ImageIcon(Assets.class.getResource("/textures/btnClass.PNG"));
		btnClassed  = new ImageIcon(Assets.class.getResource("/textures/btnClassed.PNG"));
		
		btnUseCase =  new ImageIcon(Assets.class.getResource("/textures/btnUseCase.PNG"));
		btnUseCased =  new ImageIcon(Assets.class.getResource("/textures/btnUseCased.PNG"));
	}
}
