package ncu.csie.uml;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.JPanel;

import ncu.csie.uml.mode.Mode;
import ncu.csie.uml.shape.BasicObject;
import ncu.csie.uml.shape.UMLShape;
import ncu.csie.uml.shape.BasicObjectManager;

public class UMLJPanel extends JPanel implements MouseListener{
	
	
	

	public UMLJPanel() {		
		addMouseListener(this);
	}

	@Override
	public void mouseClicked(MouseEvent evt) {
		if(Mode.getMode() != null){
			Mode.getMode().mouseClicked(evt);
			repaint();
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(Mode.getMode() != null){
			Mode.getMode().mousePressed(e);			
			repaint();
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if(Mode.getMode() != null){
			Mode.getMode().mouseReleased(e);
			repaint();
		}
		
	}

	@Override
	public void paint(Graphics g){
		super.paint(g);
		for(BasicObject obj: BasicObjectManager.getInstance().getBasicObjects()){
			obj.draw((Graphics2D)g);
		}
	}
	
}
