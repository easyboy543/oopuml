package ncu.csie.uml;

import java.awt.event.ActionEvent;

import ncu.csie.uml.gfx.Assets;
import ncu.csie.uml.mode.GenLineMode;
import ncu.csie.uml.mode.Mode;

public class GenLineBtn extends UMLBtn {

	public GenLineBtn(String name) {
		super(name);
		resetIcon();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		UMLBtnsGroup.resetIcons();
		setIcon(Assets.btnGenLined);
		Mode.setMode(new GenLineMode());
	}

	@Override
	public void resetIcon() {
		setIcon(Assets.btnGenLine);
	}

}
