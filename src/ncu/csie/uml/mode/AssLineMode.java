package ncu.csie.uml.mode;

import java.awt.event.MouseEvent;
import java.util.Vector;

import ncu.csie.uml.shape.AssLineShape;
import ncu.csie.uml.shape.BasicObject;
import ncu.csie.uml.shape.BasicObjectManager;
import ncu.csie.uml.shape.LineShape;
import ncu.csie.uml.shape.Port;
import ncu.csie.uml.shape.UMLShape;

public class AssLineMode extends Mode {

	private Port sd, ed;

	@Override
	public void mouseClicked(MouseEvent evt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		sd = BasicObjectManager.getInstance().getClosestPort(evt.getPoint());
		
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		ed = BasicObjectManager.getInstance().getClosestPort(evt.getPoint());
		if(sd != null && ed != null){
			ed.addLine(new AssLineShape(sd, ed));			
		}
		
	}	
	
	
	

}
