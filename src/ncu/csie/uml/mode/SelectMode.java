package ncu.csie.uml.mode;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.Vector;

import ncu.csie.uml.shape.BasicObject;
import ncu.csie.uml.shape.BasicObjectManager;
import ncu.csie.uml.shape.UMLShape;

public class SelectMode extends Mode {

	private Point sp, ep;
	
	@Override
	public void mouseClicked(MouseEvent evt) {		
		
		
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		sp = evt.getPoint();
		BasicObjectManager.getInstance().singleSelect(evt.getPoint());
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		ep = evt.getPoint();
		if(sp.equals(ep)){
			return ;
		}		
		BasicObjectManager.getInstance().multiSelect(sp, ep);
		BasicObjectManager.getInstance().moveBasicObject(sp, ep);
		
	}
		

}
