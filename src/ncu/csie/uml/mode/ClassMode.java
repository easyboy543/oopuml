package ncu.csie.uml.mode;

import java.awt.event.MouseEvent;
import java.util.Vector;

import ncu.csie.uml.shape.BasicObject;
import ncu.csie.uml.shape.BasicObjectManager;
import ncu.csie.uml.shape.ClassShape;
import ncu.csie.uml.shape.UMLShape;

public class ClassMode extends Mode {

	@Override
	public void mouseClicked(MouseEvent evt) {
		BasicObjectManager.getInstance().addBasicObject(new ClassShape(evt.getX(), evt.getY()));		
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		// TODO Auto-generated method stub
		
	}

	

	

}
