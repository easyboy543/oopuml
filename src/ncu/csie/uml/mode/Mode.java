package ncu.csie.uml.mode;

import java.awt.event.MouseEvent;
import java.util.Vector;

import ncu.csie.uml.shape.BasicObject;
import ncu.csie.uml.shape.UMLShape;
import ncu.csie.uml.shape.BasicObjectManager;

public abstract class Mode {
	private static Mode currentMode = null;
	
	public static void setMode(Mode mode){
		currentMode = mode;
	}
	
	public static  Mode getMode(){
		return currentMode;
	}
	
	public abstract void mouseClicked(MouseEvent evt);
	public abstract void mousePressed(MouseEvent evt);
	public abstract void mouseReleased(MouseEvent evt);
}
