package ncu.csie.uml.mode;

import java.awt.event.MouseEvent;

import ncu.csie.uml.shape.BasicObjectManager;
import ncu.csie.uml.shape.ComLineShape;
import ncu.csie.uml.shape.GenLineShape;
import ncu.csie.uml.shape.Port;

public class ComLineMode extends Mode {

	private Port sd, ed;		

	

	@Override
	public void mouseClicked(MouseEvent evt) {
		

	}

	@Override
	public void mousePressed(MouseEvent evt) {
		sd = BasicObjectManager.getInstance().getClosestPort(evt.getPoint());

	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		ed = BasicObjectManager.getInstance().getClosestPort(evt.getPoint());
		if(sd != null && ed != null){
			ed.addLine(new ComLineShape(sd, ed));			
		}

	}

}
